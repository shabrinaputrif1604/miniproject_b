﻿using med_id.datamodels;
using med_id.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace med_id.api.Controllers
{
    [Route("api/[controller]")]     
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse response = new VMResponse();
        public AuthController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        //public class LoginRequest
        //{
        //    public string Email { get; set; }
        //    public string Password { get; set; }
        //}

        [HttpGet("Login/{email}/{password}")]
        public VMResponse CheckLogin(string email, string password)
        {
            MUser dataUser = db.MUsers.Where(a => a.Email == email).FirstOrDefault();
            if(dataUser == null)
            {
                response.Success = false;
                response.Message = "Invalid email/password";

                return response;
            }

            if (dataUser.IsLocked == true)
            {
                response.Success = false;
                response.Message = "Account locked";

                return response;
            }

            if(dataUser.Password != password)
            {
                dataUser.LoginAttempt++;
                if(dataUser.LoginAttempt >= 3)
                {
                    dataUser.IsLocked = true;
                }

                db.MUsers.Update(dataUser);
                db.SaveChanges();
    
                response.Success = false;
                response.Message = "Invalid email/password";

                return response;
            }

            VMBiodataUser data = (from u in db.MUsers
                                  join b in db.MBiodata on u.BiodataId equals b.Id
                                  join r in db.MRoles on u.RoleId equals r.Id
                                  where u.IsDelete == false && u.Email == email && u.Password == password && u.IsLocked == false
                                  select new VMBiodataUser
                                  {
                                      Id = u.Id,
                                      BiodataId = b.Id,
                                      RoleId = u.RoleId,
                                      NameUser = b.Fullname ?? "",
                                      NameRole = r.Name ?? "",
                                  }).FirstOrDefault()!;

            dataUser.LastLogin = DateTime.Now;

            db.MUsers.Update(dataUser);
            db.SaveChanges();

            response.Success = true;
            response.Message = "Login success";
            response.Entity = data;

            return response;

            //if (dataUser.Email != null)
            //{
            //    if(dataUser.Password == password)
            //    {
            //        dataUser.LastLogin = DateTime.Now;
            //        dataUser.LoginAttempt = 0;

            //    }
            //    else
            //    {
            //        if(dataUser.LoginAttempt < 3)
            //        {
            //            dataUser.LoginAttempt += 1;
            //        }
            //        else
            //        {
            //            dataUser.IsLocked = true;
            //        }
            //    }
            //    db.MUsers.Update(dataUser);
            //    db.SaveChanges();
            //}
            //else
            //{
                
            //}
            ////Console.WriteLine("Login attempt " + dataUser.LoginAttempt.ToString());
            
            //return data;
        }

        [HttpGet("Menu/{idRole}")]
        public List<VMMenu> MenuList(int idRole)
        {
            List<VMMenu> menu = (from parent in db.MMenus
                           join menus in db.MMenuRoles on parent.Id equals menus.MenuId
                           where parent.ParentId == null && menus.RoleId == idRole && parent.IsDelete == false && menus.IsDelete == false
                           select new VMMenu
                           {
                               Id = parent.Id,
                               Name = parent.Name,
                               Url = parent.Url,
                               BigIcon = parent.BigIcon,
                               SmallIcon = parent.SmallIcon,
                               RoleId = idRole,
                               listChild = (from child in db.MMenus
                                            join menu_child in db.MMenuRoles on child.Id equals menu_child.MenuId
                                            where child.ParentId == parent.Id && child.IsDelete == false && menu_child.IsDelete == false && menu_child.RoleId == idRole
                                            select new VMMenu
                                            {
                                                Id = child.Id,
                                                Name = child.Name,
                                                Url = child.Url,
                                                BigIcon = child.BigIcon,
                                                SmallIcon = child.SmallIcon,
                                                RoleId = idRole
                                            }).ToList()
                           }
                           ).ToList();
            return menu;
        }
    }
}
