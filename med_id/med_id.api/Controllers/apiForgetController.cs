﻿using med_id.datamodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mail;
using System.Net;
using med_id.viewmodels;
using System.Drawing;
using System.Diagnostics.CodeAnalysis;
using static System.Net.WebRequestMethods;

namespace med_id.api.Controllers
{
    [Route("Forget")]
    [ApiController]
    public class apiForgetController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();

        public apiForgetController(DB_SpecificationContext _db)
        {
            db = _db;
        }
        [HttpGet("GetAllData")]
        public List<MUser> GetAllData()
        {
            List<MUser> data = db.MUsers.Where(a => a.IsDelete == false).ToList();
            return data;
        }
        [HttpGet("CheckEmail/{email}")]
        public bool CheckEmail(string email)
        {
            MUser data = new MUser();
            
            data = db.MUsers.Where(a => a.Email == email && a.IsDelete == false).FirstOrDefault();
            

            if (data != null)
            {
                return true;
            }

            return false;
        }
        [HttpPost("Save")]
        public VMResponse Save(TToken dataParam)
        {
            List<TToken> dataCount = db.TTokens.Where(a => a.Email == dataParam.Email && a.IsDelete == false
            && a.IsExpired == false).ToList();
            if (dataCount != null)
            {
                foreach (var item in dataCount)
                {
                    item.ModifiedOn = DateTime.Now;
                    item.ModifiedBy = 2;
                    item.IsExpired = true;


                    try
                    {
                        db.Update(item);
                        db.SaveChanges();

                        respon.Message = "Kode Terkirim";

                    }

                    catch (Exception ex)
                    {
                        respon.Success = false;
                        respon.Message = "Failed saved" + ex.Message;


                    }
                }

            }
            TToken data = new TToken();
            data.Email = dataParam.Email;
            data.UserId = 1;
            data.Token = dataParam.Token;
            data.ExpiredOn = DateTime.Now.AddMinutes(10);
            data.IsExpired = false;

            data.UsedFor = dataParam.UsedFor;
            data.IsDelete = false;
            data.CreatedBy = 1;
            data.CreatedOn = DateTime.Now;


            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed Saved : " + ex.Message;
            }

            return respon;
        }
        [HttpPost("ReturnSend")]
        public VMResponse ReturnSend(TToken dataParam)
        {
            respon.Message = "Data success saved";
            List<TToken> dataCount = db.TTokens.Where(a => a.Email == dataParam.Email && a.IsDelete == false
            && a.IsExpired == false).ToList();
            if (dataCount != null)
            {
                foreach (var item in dataCount)
                {
                    item.ModifiedOn = DateTime.Now;
                    item.ModifiedBy = 2;
                    item.IsExpired = true;


                    try
                    {
                        db.Update(item);
                        db.SaveChanges();

                        respon.Message = "Verifikasi Berhasil";

                    }

                    catch (Exception ex)
                    {
                        respon.Success = false;
                        respon.Message = "Failed saved" + ex.Message;


                    }
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            TToken data = new TToken();
            data.Email = dataParam.Email;
            data.UserId = 1;
            data.Token = dataParam.Token;
            data.ExpiredOn = DateTime.Now.AddMinutes(10);
            data.IsExpired = false;

            data.UsedFor = dataParam.UsedFor;
            data.IsDelete = false;
            data.CreatedBy = 1;
            data.CreatedOn = DateTime.Now;
            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
                }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed Saved : " + ex.Message;
            }

            return respon;
        }
        [HttpGet("CheckEmailToken/{email}/{otp}")]
        public bool CheckEmailToken(string email, string otp)

        {
            TToken  data = new TToken();

            data = db.TTokens.Where(a => a.Email == email && a.IsDelete == false && a.Token == otp).FirstOrDefault()!;
            if (data != null)
            {
                //DateTime now = DateTime.Now;
                //int nowint = Convert.ToInt32(now.ToString("yyyyMMddHHmmss"));
                //int expint = Convert.ToInt32(data.ExpiredOn);
                //if (DateTime.Now < data.ExpiredOn)
                //{
                //    return true;
                //}
                return true;
            }
            return false;
        }

        [HttpGet("CheckTokenExp/{email}/{otp}")]
        public bool CheckTokenExp(string email, string otp)

        {
            TToken data = new TToken();

            data = db.TTokens.Where(a => a.Email == email && a.IsDelete == false && a.Token == otp && a.IsExpired == false).FirstOrDefault()!;
            if (data != null)
            {
                //DateTime now = DateTime.Now;
                //int nowint = Convert.ToInt32(now.ToString("yyyyMMddHHmmss"));
                //int expint = Convert.ToInt32(data.ExpiredOn);
                if (DateTime.Now < data.ExpiredOn)
                {
                    return true;
                }
            }
            return false;
        }
        [HttpPost("UpdateOtp")]
        public VMResponse UpdateOtp(TToken dataParam)
        {
           
            List<TToken> dataCount = db.TTokens.Where(a => a.Email == dataParam.Email && a.IsDelete == false
            && a.IsExpired == false).ToList();
            if (dataCount != null)
            {
                foreach (var item in dataCount)
                {
                    item.ModifiedOn = DateTime.Now;
                    item.ModifiedBy = 2;
                    item.IsExpired = true;


                    try
                    {
                        db.Update(item);
                        db.SaveChanges();

                        respon.Message = "Verifikasi Berhasil";

                    }

                    catch (Exception ex)
                    {
                        respon.Success = false;
                        respon.Message = "Failed saved" + ex.Message;


                    }
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;
        }
        [HttpPost("UpdatePass")]
        public VMResponse UpdatePass(VMResetPassOtp dataParam)
        {

            MUser dataPassword = db.MUsers.Where(a => a.Email == dataParam.Email && a.IsDelete == false).FirstOrDefault();
            TResetPassword resetPassword = new TResetPassword();

           
            if (dataPassword != null)
            {
                resetPassword.OldPassword = dataPassword.Password;
                resetPassword.NewPassword = dataParam.Password;
                resetPassword.CreatedBy = 1;
                resetPassword.CreatedOn = DateTime.Now;
                resetPassword.IsDelete = false;
                db.Add(resetPassword);
                db.SaveChanges();

                dataPassword.Password = dataParam.Password;
                dataPassword.ModifiedOn = DateTime.Now;
                dataPassword.ModifiedBy = 2;

                try
                {
                    db.Update(dataPassword);
                    db.SaveChanges();

                    respon.Message = "Verifikasi Berhasil";

                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved" + ex.Message;


                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;
        }
    }
}
