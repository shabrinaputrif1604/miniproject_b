﻿using med_id.datamodels;
using med_id.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace med_id.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiObatController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiObatController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        [HttpGet("GetAllDataOrderHeader")]
        public List<VMTMedicalItemPurchaseDetail> GetAllDataHeader()
        {
            List<VMTMedicalItemPurchaseDetail> data = (from t in db.TMedicalItemPurchaseDetails
                                                       join m in db.MMedicalItems on t.MedicalItemId equals m.Id
                                                       join c in db.MMedicalItemCategories on m.MedicalItemCategoryId equals c.Id
                                                       where t.IsDelete == false
                                                       select new VMTMedicalItemPurchaseDetail
                                                       {
                                                        Id = t.Id,
                                                        NameCategory = c.Name,

                                                        Name = m.Name,
                                                        Caution = m.Caution,

                                                        PriceMax = m.PriceMax,
                                                        PriceMin = m.PriceMin,

                                                        Qty = t.Qty,
                                                        SubTotal = t.SubTotal,
                                                        ImagePath = m.ImagePath,

                                                        Indication = m.Indication,
                                                       }).ToList()!;
            return data;
        }




    }
}
