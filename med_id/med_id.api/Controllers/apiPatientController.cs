﻿using med_id.datamodels;
using med_id.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NuGet.Protocol;
using System.Xml.Linq;

namespace med_id.api.Controllers
{
    [Route("api/patient")]
    [ApiController]
    public class apiPatientController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse response = new VMResponse();
        public apiPatientController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        [HttpGet]
        public List<VMFindDoctor> GetAllData()
        {
            DateTime now = DateTime.Now;
            Console.Write(now.Date.ToString("yyyy-MM-dd"));

            List<VMFindDoctor> data = (from b in db.MBiodata
                                       join d in db.MDoctors on b.Id equals d.BiodataId
                                       select new VMFindDoctor
                                       {
                                           Id = b.Id,
                                           DoctorId = d.Id,
                                           Fullname = b.Fullname,
                                           ListMedicalFacility = (from office in db.TDoctorOffices 
                                                                  join mf in db.MMedicalFacilities on office.MedicalFacilityId equals mf.Id
                                                                  join mfc in db.MMedicalFacilityCategories on mf.MedicalFacilityCategoryId equals mfc.Id
                                                                  where office.DoctorId == d.Id
                                                                  select new VMFindDoctor
                                                                  {
                                                                      Specialization = office.Specialization,
                                                                      Experience = now.Year - office.StartDate.Year,
                                                                      MedicalFacilityCategoryName = mfc.Name,
                                                                      MedicalFacilityName = mf.Name
                                                                  }).OrderByDescending(a => a.Experience).ToList()
                                       }).ToList();
            return data;
        }

        //[HttpGet]
        //public List<VMFindDoctor> GetAllData()
        //{
        //    DateTime now = DateTime.Now;
        //    Console.Write(now.Date.ToString("yyyy-MM-dd"));

        //    List<VMFindDoctor> data = (from b in db.MBiodata
        //                               join d in db.MDoctors on b.Id equals d.BiodataId
        //                               join office in db.TDoctorOffices on d.Id equals office.DoctorId
        //                               join mf in db.MMedicalFacilities on office.MedicalFacilityId equals mf.Id
        //                               join mfc in db.MMedicalFacilityCategories on mf.MedicalFacilityCategoryId equals mfc.Id
        //                               select new VMFindDoctor
        //                               {
        //                                   Id = b.Id,
        //                                   DoctorId = d.Id,
        //                                   Fullname = b.Fullname,
        //                                   Specialization = office.Specialization,
        //                                   MedicalFacilityName = mf.Name,
        //                                   MedicalFacilityCategoryName = mfc.Name,
        //                                   Experience = now.Year - office.StartDate.Year
        //                               }).ToList();
        //    return data;
        //}

        [HttpGet("{id}")]
        public VMFindDoctor GetDataById(int id)
        {
            DateTime now = DateTime.Now;
            //Console.Write(now.Date.ToString("yyyy-MM-dd"));

            VMFindDoctor data = (from b in db.MBiodata
                                join d in db.MDoctors on b.Id equals d.BiodataId
                                where d.Id == id
                                select new VMFindDoctor
                                {
                                    Id = b.Id,
                                    DoctorId = d.Id,
                                    Fullname = b.Fullname,
                                    ListMedicalFacility = (from office in db.TDoctorOffices
                                                            join mf in db.MMedicalFacilities on office.MedicalFacilityId equals mf.Id
                                                            join mfc in db.MMedicalFacilityCategories on mf.MedicalFacilityCategoryId equals mfc.Id
                                                           join dot in db.TDoctorOfficeTreatments on office.Id equals dot.DoctorOfficeId
                                                           where office.DoctorId == d.Id
                                                            select new VMFindDoctor
                                                            {
                                                                DoctorOfficeId = office.Id,
                                                                MedicalFacilityId = mf.Id,
                                                                DoctorOfficeTreatmentId = dot.Id,
                                                                Specialization = office.Specialization,
                                                                Experience = now.Year - office.StartDate.Year,
                                                                MedicalFacilityCategoryName = mfc.Name,
                                                                MedicalFacilityName = mf.Name
                                                            }).OrderByDescending(a => a.Experience).ToList()
                                }).FirstOrDefault()!;
            return data;
        }

        [HttpGet("family/{idParent}")]
        public List<VMFamilyMember> GetDataByIdParent(int idParent)
        {
            List<VMFamilyMember> data = (from b in db.MBiodata 
                                         join c in db.MCustomers on b.Id equals c.BiodataId
                                         join cm in db.MCustomerMembers on c.Id equals cm.CustomerId
                                         join cr in db.MCustomerRelations on cm.CustomerRelationId equals cr.Id
                                        where cm.ParentBiodataId == idParent
                                        select new VMFamilyMember
                                        {
                                            Id = c.Id,
                                            Name = b.Fullname ?? "",
                                            RelationName = cr.Name,
                                        }).ToList();
            return data;
        }

        [HttpGet("medicalFacility/{id}")]
        public VMFindDoctor GetDataMedicalFacility(int id)
        {
            VMFindDoctor data = (from b in db.MBiodata
                                 join d in db.MDoctors on b.Id equals d.BiodataId
                                 where d.Id == id
                                 select new VMFindDoctor
                                {
                                    Id = b.Id,
                                    DoctorId = d.Id,
                                    Fullname = b.Fullname,
                                    ListMedicalFacility = (from office in db.TDoctorOffices
                                                            join mf in db.MMedicalFacilities on office.MedicalFacilityId equals mf.Id
                                                            join mfc in db.MMedicalFacilityCategories on mf.MedicalFacilityCategoryId equals mfc.Id
                                                            where office.DoctorId == d.Id
                                                            select new VMFindDoctor
                                                            {
                                                                DoctorOfficeId = office.Id,
                                                                MedicalFacilityId = mf.Id,
                                                                MedicalFacilityCategoryName = mfc.Name,
                                                                MedicalFacilityName = mf.Name
                                                            }).ToList()
                                }).FirstOrDefault()!;

            return data;
        }

        [HttpGet("treatment/{id}")]
        public VMTreatment GetDataTreatmentById(int id)
        {
            VMTreatment data = (from b in db.MBiodata
                                join d in db.MDoctors on b.Id equals d.BiodataId
                                where d.Id == id
                                select new VMTreatment
                                {
                                    Id = d.Id,
                                    treatments = (from dt in db.TDoctorTreatments
                                                  join dot in db.TDoctorOfficeTreatments on dt.Id equals dot.DoctorTreatmentId
                                                  where dt.DoctorId == d.Id
                                                  select new VMTreatment
                                                  {
                                                      DoctorOfficeTreatmentId = dot.Id,
                                                      TreatmentName = dt.Name
                                                  }).ToList()
                                }).FirstOrDefault()!;
            return data;
        }

        [HttpGet("schedule/{id}/{medicalFacilityId}")]
        public List<VMSchedule> GetDataDaysById(int id, int medicalFacilityId)
        {
            List<VMSchedule> data = (from mfs in db.MMedicalFacilitySchedules
                                     join mf in db.MMedicalFacilities on mfs.MedicalFacilityId equals mf.Id
                                     join dos in db.TDoctorOfficeSchedules on mfs.Id equals dos.MedicalFacilityScheduleId
                                     where dos.DoctorId == id && mf.Id == medicalFacilityId
                                     select new VMSchedule
                                     {
                                         IdDay = dos.Id,
                                         Day = mfs.Day
                                     }).ToList();
            return data;
        }

        [HttpPost("makeAppointment")]
        public VMResponse MakeAppointment(VMFindDoctor dataParam)
        {
            TAppointment data = new TAppointment();
            data.CustomerId = dataParam.CustomerId;
            data.DoctorOfficeId = dataParam.DoctorOfficeId;
            data.DoctorOfficeTreatmentId = dataParam.DoctorOfficeTreatmentId;
            data.DoctorOfficeScheduleId = dataParam.DoctorOfficeScheduleId;
            data.AppointmentDate = dataParam.AppointmentDate;
            data.CreatedBy = 1;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.TAppointments.Add(data);
                db.SaveChanges();

                response.Message = "Success make appointment";
                response.Entity = data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "Fail to make appointment on " + ex.Message;
            }

            return response;
        }

        [HttpGet("fullSlotDate/{id}")]
        public List<VMFullSlotDate> GetFullSlotDate(int id)
        {
            List<VMFullSlotDate> data = (from appointment in db.TAppointments
                                            join schedule in db.TDoctorOfficeSchedules
                                            on appointment.DoctorOfficeScheduleId equals schedule.Id
                                            where schedule.DoctorId == id
                                            group new { appointment, schedule } by new
                                            {
                                                appointment.DoctorOfficeScheduleId,
                                                schedule.Slot,
                                                appointment.AppointmentDate
                                            } into grouped
                                            where grouped.Count() >= grouped.Key.Slot
                                            select new VMFullSlotDate
                                            {
                                                DoctorOfficeScheduleId = grouped.Key.DoctorOfficeScheduleId,
                                                SlotNow = grouped.Count(),
                                                Slot = grouped.Key.Slot,
                                                AppointmentDate = grouped.Key.AppointmentDate
                                            }).ToList();
            
            return data;

        }

        [HttpGet("appointments/{id}")]
        public List<VMAppointments> GetAppointments (int id)
        {
            List<VMAppointments> data = (from c in db.MCustomers
                                         join cm in db.MCustomerMembers on c.Id equals cm.CustomerId
                                         join b in db.MBiodata on c.BiodataId equals b.Id
                                         join a in db.TAppointments on c.Id equals a.CustomerId
                                         join tdo in db.TDoctorOffices on a.DoctorOfficeId equals tdo.Id
                                         join mf in db.MMedicalFacilities on tdo.MedicalFacilityId equals mf.Id
                                         join dos in db.TDoctorOfficeSchedules on a.DoctorOfficeScheduleId equals dos.Id
                                         join mfs in db.MMedicalFacilitySchedules on dos.MedicalFacilityScheduleId equals mfs.Id
                                         join dot in db.TDoctorOfficeTreatments on a.DoctorOfficeTreatmentId equals dot.Id
                                         join dt in db.TDoctorTreatments on dot.DoctorTreatmentId equals dt.Id
                                         where cm.ParentBiodataId == id
                                         select new VMAppointments
                                         {
                                             IdAppointment = a.Id,
                                             IdDoctor = tdo.DoctorId,
                                             Fullname = b.Fullname,
                                             AppointmentDate = a.AppointmentDate,
                                             ScheduleStart = mfs.TimeScheduleStart,
                                             ScheduleEnd = mfs.TimeScheduleEnd,
                                             MedicalFacility = mf.Name,
                                             Treatment = dt.Name
                                         }).ToList();

            return data;
        }

        [HttpGet("appointment/{idAppointment}")]
        public VMAppointments AppointmentById(int idAppointment)
        {
            VMAppointments data = (from a in db.TAppointments
                                   where a.Id == idAppointment
                                   select new VMAppointments
                                   {
                                       IdAppointment = a.Id,
                                       IdDoctorOfficeTreatment = a.DoctorOfficeTreatmentId,
                                       IdCustomer = a.CustomerId,
                                       IdDoctorOffice = a.DoctorOfficeId,
                                       AppointmentDate = a.AppointmentDate
                                   }).FirstOrDefault()!;

            return data;
        }

        [HttpPut("editAppointment/{idAppointment}")]
        public VMResponse EditAppointment(long idAppointment, VMFindDoctor dataParam)
        {
            TAppointment data = db.TAppointments.Find(idAppointment);
            data.CustomerId = dataParam.CustomerId;
            data.DoctorOfficeId = dataParam.DoctorOfficeId;
            data.DoctorOfficeTreatmentId = dataParam.DoctorOfficeTreatmentId;
            data.DoctorOfficeScheduleId = dataParam.DoctorOfficeScheduleId;
            data.AppointmentDate = dataParam.AppointmentDate;
            data.CreatedBy = 1;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.TAppointments.Update(data);
                db.SaveChanges();

                response.Message = "Success make appointment";
                response.Entity = data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "Fail to make appointment on " + ex.Message;
            }

            return response;
        }
    }
}
