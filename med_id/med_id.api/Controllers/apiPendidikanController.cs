﻿using med_id.datamodels;
using med_id.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace med_id.api.Controllers
{
    [Route("Pendidikan")]
    [ApiController]
    public class apiPendidikanController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();

        public apiPendidikanController(DB_SpecificationContext _db)
        {
            db = _db;
        }
        [HttpGet("GetAllData")]
        public List<MEducationLevel> GetAllData()
        {
            List<MEducationLevel> data = db.MEducationLevels.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]

        public MEducationLevel DataById(int id)
        {
            MEducationLevel result = db.MEducationLevels.Where(a => a.Id == id).FirstOrDefault();
            return result;
        }

        [HttpGet("CheckName/{name}/{id}")]

        public bool CheckName(string name, int id)
        {
            MEducationLevel data = new MEducationLevel();

            if (id == 0) //untuk create
            {
                data = db.MEducationLevels.Where(a => a.Name == name && a.IsDelete == false).FirstOrDefault();
            }
            else //untuk edit
            {
                data = db.MEducationLevels.Where(a => a.Name == name && a.IsDelete == false && a.Id != id).FirstOrDefault();
            }

            if (data != null)
            {
                return true;
            }

            return false;
        }

        [HttpPost("Save")]
        public VMResponse Save(MEducationLevel data)
        {
            data.CreatedBy = 1;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saves";

            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Faild saved" + ex.Message;

            }
            return respon;
        }

        [HttpPut("Edit")]

        public VMResponse Edit(MEducationLevel data)
        {
            MEducationLevel dt = db.MEducationLevels.Where(a => a.Id == data.Id).FirstOrDefault();

            if (dt != null)
            {
                dt.Name= data.Name;
                dt.ModifiedBy = 1;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";

                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved" + ex.Message;


                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;
        }

        [HttpDelete("Delete/{id}/{modifedby}")]

        public VMResponse Delete(int id, int modifedby)
        {
            MEducationLevel dt = db.MEducationLevels.Where(a => a.Id == id).FirstOrDefault();
            if (dt != null)
            {
                dt.IsDelete = true;
                dt.ModifiedBy = modifedby;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success deleted";

                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved" + ex.Message;


                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;


        }
    }
}
