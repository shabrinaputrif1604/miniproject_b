﻿using med_id.datamodels;
using med_id.viewmodels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace med_id.api.Controllers
{
    [Route("Profile")]
    [ApiController]
    public class apiProfileController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;


        public apiProfileController(DB_SpecificationContext _db)
        {
            db = _db;
        }
        [HttpGet("GetDataProfilDoctorById/{id}")]
        public VMDoctorProfile GetDataProfilDoctorById(int id)
        {
            VMDoctorProfile data = (from a in db.MDoctors
                                    join b in db.MBiodata on a.BiodataId equals b.Id
                                    join c in db.TCurrentDoctorSpecializations on a.Id equals c.DoctorId
                                    //join d in db.MSpecializations on c.SpecializationId equals d.Id
                                    where a.IsDelete == false && a.Id == id
                                    select new VMDoctorProfile
                                    {
                                        //Dokter Profil 
                                        DoctorId = a.Id,
                                        Img = b.ImagePath,
                                        NameDoctor = b.Fullname,
                                        //Specialization = d.Name,

                                        CurrentId = c.SpecializationId,

                                        NameSpesialis = (from t in db.TCurrentDoctorSpecializations
                                                         join s in db.MSpecializations on t.SpecializationId equals s.Id
                                                         where t.DoctorId == a.Id
                                                         select new VMDoctorProfile
                                                         {                                                            
                                                            Specialization = s.Name,
                                                         }).ToList(),
                                        //List Jumlah Janji
                                        ListJumlahJanji = (from ta in db.TAppointments
                                                           join tdo in db.TDoctorOffices on ta.DoctorOfficeId equals tdo.Id
                                                           where tdo.DoctorId == a.Id && ta.IsDelete == false
                                                           select new VMDoctorProfile
                                                           {
                                                               IdAppointment = ta.Id
                                                           }).ToList(),
                                        ListJanjiDone = (from tap in db.TAppointments
                                                         join doff in db.TDoctorOffices on tap.DoctorOfficeId equals doff.Id
                                                         join tappd in db.TAppointmentDones on tap.Id equals tappd.AppointmentId
                                                         where doff.DoctorId == a.Id && tap.IsDelete == false
                                                         select new VMDoctorProfile
                                                         {
                                                             IdAppointmentDone = tappd.Id
                                                         }
                                                           ).ToList(),
                                        ListJanjiCancel = (from tap in db.TAppointments
                                                           join doff in db.TDoctorOffices on tap.DoctorOfficeId equals doff.Id
                                                           join tapc in db.TAppointmentCancellations on tap.Id equals tapc.AppointmentId
                                                           where doff.DoctorId == a.Id && tap.IsDelete == false
                                                           select new VMDoctorProfile
                                                           {
                                                               IdAppointmentCancel = tapc.Id
                                                           }).ToList(),

                                        //List Konsultasi
                                        ListKonsultasi = (from mdoc in db.TCustomerChats
                                                          where mdoc.DoctorId == a.Id && mdoc.IsDelete == false
                                                          select new VMDoctorProfile
                                                          {
                                                              IdKonsul = mdoc.Id
                                                          }).ToList(),

                                        //List Tindakan Medis
                                        ListNameTreatment = (from d in db.TDoctorTreatments
                                                             where d.DoctorId == a.Id && d.IsDelete == false
                                                             select new VMDoctorProfile
                                                             {
                                                                 NameTreatment = d.Name
                                                             }).ToList(),

                                        //List Riwayat Praktek
                                        ListNameHospital = (from td in db.TDoctorOffices
                                                            join mm in db.MMedicalFacilities on td.MedicalFacilityId equals mm.Id
                                                            where td.DoctorId == a.Id && td.IsDelete == false
                                                            select new VMDoctorProfile
                                                            {
                                                                IdNameHospital = mm.Id,
                                                                NameHospital = mm.Name,
                                                                SpecializationFaskes = td.Specialization,
                                                                StartDate = td.StartDate,
                                                                EndDate = td.EndDate
                                                            }).ToList(),

                                        //List Pendidikan
                                        ListInstitutionName = (from eee in db.MDoctorEducations
                                                               where eee.DoctorId == a.Id && eee.IsDelete == false
                                                               select new VMDoctorProfile
                                                               {
                                                                   InstitutionName = eee.InstitutionName,
                                                                   Major = eee.Major,
                                                                   Lulus = eee.EndYear
                                                               }).OrderBy(a => a.InstitutionName).ToList(),

                                        //List Lokasi Praktek
                                        ListLokasiPraktek = (from td in db.TDoctorOffices
                                                             join mm in db.MMedicalFacilities on td.MedicalFacilityId equals mm.Id
                                                             join dot in db.TDoctorOfficeTreatments on td.Id equals dot.DoctorOfficeId
                                                             join dotp in db.TDoctorOfficeTreatmentPrices on dot.Id equals dotp.DoctorOfficeTreatmentId
                                                             where td.DoctorId == a.Id && td.IsDelete == false && td.EndDate == null
                                                             select new VMDoctorProfile
                                                             {
                                                                 IdNameHospital = mm.Id,
                                                                 PriceStartFrom = dotp.PriceStartFrom,
                                                                 NameHospital = mm.Name,
                                                                 SpecializationFaskes = td.Specialization,
                                                                 Lokasi = mm.FullAddress,

                                                                 //List Jadwal Praktek Waktu
                                                                 ListJadwalPraktekWaktu = (from dos in db.TDoctorOfficeSchedules
                                                                                           join ac in db.MMedicalFacilitySchedules on dos.MedicalFacilityScheduleId equals ac.Id
                                                                                           where dos.DoctorId == a.Id && ac.MedicalFacilityId == mm.Id && dos.IsDelete == false
                                                                                           select new VMDoctorProfile
                                                                                           {
                                                                                               Day = ac.Day,
                                                                                               JamMulai = ac.TimeScheduleStart,
                                                                                               JamSelesai = ac.TimeScheduleEnd
                                                                                           }).ToList()
                                                             }).ToList(),

                                    }).FirstOrDefault()!;
            return data;
        }

        [HttpPost("Save")]
        public VMResponse Save(MBiodatum data)
        {
            MDoctor doctor = db.MDoctors.Where(a => a.Id == data.Id).FirstOrDefault()!;
            MBiodatum dt = db.MBiodata.Where(a => a.Id == doctor.BiodataId && a.IsDelete == false).FirstOrDefault()!;

            dt.ImagePath = data.ImagePath;
            dt.IsDelete = false;
            dt.ModifiedBy = 1;
            dt.ModifiedOn = DateTime.Now;
            try
            {
                db.Update(dt);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed Saved : " + ex.Message;
            }

            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TCurrentDoctorSpecialization data)
        {
            TCurrentDoctorSpecialization dt = db.TCurrentDoctorSpecializations.Where(a => a.DoctorId == data.DoctorId).FirstOrDefault()!;

            if (dt != null)
            {
                dt.SpecializationId = data.SpecializationId;
                dt.ModifiedBy = IdUser;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed Saved : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }

        [HttpGet("GetDataDoctor/{idBio}")]

        public MDoctor GetDataDoctor(int idBio)
        {
            MDoctor data = new MDoctor();
            data = db.MDoctors.Where(a => a.BiodataId == idBio && a.IsDelete == false).FirstOrDefault();

            return data;
        }
        [HttpGet("CheckJadwal/{id}/{nameDay}/{nameJam}/{nameMenit}")]

        public bool CheckJadwal(int id, string nameDay, int nameJam, int nameMenit)
        {
            List<VMDoctorProfile> dataCount = (from td in db.TDoctorOffices
                                               join mm in db.MMedicalFacilities on td.MedicalFacilityId equals mm.Id
                                               join dot in db.TDoctorOfficeTreatments on td.Id equals dot.DoctorOfficeId
                                               join dotp in db.TDoctorOfficeTreatmentPrices on dot.Id equals dotp.DoctorOfficeTreatmentId
                                               where td.DoctorId == id && td.IsDelete == false && td.EndDate == null
                                               select new VMDoctorProfile
                                               {
                                                   IdNameHospital = mm.Id,
                                                   PriceStartFrom = dotp.PriceStartFrom,
                                                   NameHospital = mm.Name,
                                                   SpecializationFaskes = td.Specialization,
                                                   Lokasi = mm.FullAddress,

                                                   //List Jadwal Praktek Waktu
                                                   ListJadwalPraktekWaktu = (from dos in db.TDoctorOfficeSchedules
                                                                             join ac in db.MMedicalFacilitySchedules on dos.MedicalFacilityScheduleId equals ac.Id
                                                                             where dos.DoctorId == id && ac.MedicalFacilityId == mm.Id && dos.IsDelete == false
                                                                             select new VMDoctorProfile
                                                                             {
                                                                                 Day = ac.Day,
                                                                                 JamMulai = ac.TimeScheduleStart,
                                                                                 JamSelesai = ac.TimeScheduleEnd
                                                                             }).ToList()
                                               }).ToList();
                //from td in db.TDoctorOfficeSchedules
            //                                   join mm in db.MMedicalFacilitySchedules on td.MedicalFacilityScheduleId equals mm.Id
            //                                   join tdo in db.TDoctorOffices on 
            //                                   where td.DoctorId == id && td.IsDelete == false 
            //                                   select new VMDoctorProfile
            //                                   {
            //                                       IdNameHospital = mm.Id,
            //                                       Day = mm.Day,
            //                                       JamMulai = mm.TimeScheduleStart,
            //                                       JamSelesai = mm.TimeScheduleEnd
            //                                   }).ToList();


            if (dataCount != null)
            {
                foreach (var dt in dataCount)
                { 
                    foreach(var item in dt.ListJadwalPraktekWaktu)
                    if (item.Day == nameDay)
                    {
                        var waktuMulai = item.JamMulai.Split(".");
                        var waktuSelesai = item.JamSelesai.Split(".");
                        var jamMulai = Convert.ToInt32(waktuMulai[0]);
                        var menitMulai = Convert.ToInt32(waktuMulai[1]);
                        var jamSelesai = Convert.ToInt32(waktuSelesai[0]);
                        var menitSelesai = Convert.ToInt32(waktuSelesai[1]);

                        if (jamMulai == nameJam)
                        {
                            if (menitMulai <= nameMenit)
                            {

                                if (jamSelesai == nameJam)
                                {
                                    if (menitSelesai >= nameMenit)
                                    {

                                        return true;
                                    }
                                }
                                else if (jamSelesai > nameJam)
                                {

                                    return true;

                                }
                            }
                        }
                        else if (jamMulai < nameJam)
                        {
                            if (jamSelesai == nameJam)
                            {
                                if (menitSelesai >= nameMenit)
                                {

                                    return true;
                                }
                            }
                            else if (jamSelesai > nameJam)
                            {

                                return true;

                            }
                        }
                    }


                }

            }

            return false;
        }
    }
}
