﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace med_id.viewmodels
{
    public class VMAppointments
    {
        public long? IdCustomer { get; set; }
        public long? IdAppointment { get; set; }
        public long? IdDoctor { get; set; }
        public long? IdDoctorOffice { get; set; }
        public long? IdDoctorOfficeSchedule { get; set; }
        public long? IdDoctorTreatment { get; set; }
        public long? IdDoctorOfficeTreatment { get; set; }
        public string Fullname { get; set; }
        public DateTime? AppointmentDate {  get; set; }
        public string MedicalFacility {  get; set; }
        public string Treatment {  get; set; }
        public string ScheduleStart { get; set; }
        public string ScheduleEnd { get; set; }

    }
}
