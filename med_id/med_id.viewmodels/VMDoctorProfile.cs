﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace med_id.viewmodels
{
    public class VMDoctorProfile
    {
        public string? NameDoctor { get; set; }
        public long? IdDoctor { get; set; }
        public long? SpecializationId { get; set; }
        public long? CurrentId { get; set; }
        public string? Lulus { get; set; }
        public string? NameLevelLocation { get; set; }
        public string? Img { get; set; }
        public IFormFile ImageFile { get; set; }
        public string? NameTreatment { get; set; }
        public string? NameHospital { get; set; }
        public long? IdNameHospital { get; set; }
        public List<VMDoctorProfile> NameSpesialis { get; set; }
        public List<VMDoctorProfile> ListNameTreatment { get; set; }
        public List<VMDoctorProfile> ListJadwalPraktekWaktu { get; set; }
        public List<VMDoctorProfile> ListJadwalPraktek { get; set; }
        public List<VMDoctorProfile> ListDoctorName { get; set; }
        public List<VMDoctorProfile> ListKonsultasi { get; set; }
        public List<VMDoctorProfile> ListNameHospital { get; set; }
        public List<VMDoctorProfile> ListLokasiPraktek { get; set; }
        public List<VMDoctorProfile> ListSpecializationFaskes { get; set; }
        public List<VMDoctorProfile> ListInstitutionName { get; set; }
        public List<VMDoctorProfile> ListJanjiDone { get; set; }
        public List<VMDoctorProfile> ListJanjiCancel { get; set; }
        public string? SpecializationFaskes { get; set; }
        public string? InstitutionName { get; set; }
        public string? Major { get; set; }
        public long? DoctorId { get; set; }
        public long? IdAppointment { get; set; }
        public long? IdAppointmentCancel { get; set; }
        public long? IdAppointmentDone { get; set; }
        public long? IdKonsul { get; set; }
        public List<VMDoctorProfile> ListJumlahJanji { get; set; }
        public long? MedicalFacilityId { get; set; }
        public string? Specialization { get; set; }
        public string? JamMulai { get; set; }
        public string? JamSelesai { get; set; }
        public string? Day { get; set; }
        public string? Lokasi { get; set; }
        public decimal? PriceStartFrom { get; set; }
        public decimal? Price { get; set; }
        public string? Jadwal { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDelete { get; set; }
    }
}
