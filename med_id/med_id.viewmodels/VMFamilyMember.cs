﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace med_id.viewmodels
{
    public class VMFamilyMember
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string? RelationName { get; set; }
        public long BiodataId { get; set; }
    }
}
