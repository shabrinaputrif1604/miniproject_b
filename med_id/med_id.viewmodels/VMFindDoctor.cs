﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace med_id.viewmodels
{
    public class VMFindDoctor
    {
        public long Id { get; set; }
        public long BiodataId { get; set; }
        public long DoctorId { get; set; }
        public long CustomerId { get; set; }
        public long DoctorOfficeId { get; set; }
        public string? Str { get; set; }
        public string? Fullname { get; set; }
        public string? Specialization { get; set; } = null!;
        public List<VMFindDoctor>? ListMedicalFacility { get; set; }
        public long MedicalFacilityId { get; set; }
        public string? MedicalFacilityCategoryName { get; set; }
        public string? MedicalFacilityName { get; set; }
        public int Experience { get; set; }
        public long DoctorOfficeTreatmentId {  get; set; }
        public long DoctorOfficeScheduleId {  get; set; }
        public long DoctorTreatmentId {  get; set; }
        public DateTime AppointmentDate { get; set; }
    }
}
