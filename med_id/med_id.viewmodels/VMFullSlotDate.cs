﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace med_id.viewmodels
{
    public class VMFullSlotDate
    {
        public long? DoctorOfficeScheduleId { get; set; }
        public int? Slot {  get; set; }
        public DateTime? AppointmentDate { get; set; }
        public long SlotNow { get; set; }
    }
}
