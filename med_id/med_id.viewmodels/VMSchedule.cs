﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace med_id.viewmodels
{
    public class VMSchedule
    {
        public long DoctorOfficeScheduleId { get; set; }
        public long? IdDay { get; set; }
        public string? Day { get; set; }
        public long? MedicalFacilityId { get; set; }
    }
}
