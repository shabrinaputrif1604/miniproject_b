﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace med_id.viewmodels
{
    public class VMSearchPage
    {
        public string? CodeTransaction { get; set; }
        public string? NameProduct { get; set; }
        public string? Caution { get; set; }
        public long? MedicalItemCategoryId { get; set; }
        public string? NameCategory { get; set; }
        public string? Name { get; set; }
        public DateTime? MinDate { get; set; }
        public DateTime? MaxDate { get; set; }
        public decimal? MinAmount { get; set; }
        public decimal? MaxAmount { get; set; }
        public long? PriceMax { get; set; }
        public long? PriceMin { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? CurrentPageSize { get; set; }
    }
}
