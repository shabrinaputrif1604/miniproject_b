﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace med_id.viewmodels
{
    public class VMTMedicalItemPurchaseDetail
    {
        public long Id { get; set; }
        public long? MedicalItemPurchaseId { get; set; }
        public long? MedicalItemId { get; set; }
        public string? Name { get; set; }
        public string? NameCategory { get; set; }
        public string? NameSegmentation { get; set; }
        public long? MedicalItemCategoryId { get; set; }
        public string? Composition { get; set; }
        public long? MedicalItemSegmentationId { get; set; }
        public string? Manufacturer { get; set; }
        public string? Indication { get; set; }
        public int TotalQty { get; set; }
        public decimal Amount { get; set; }
        public string? Dosage { get; set; }
        public string? Directions { get; set; }
        public string? Contraindication { get; set; }
        public string? Caution { get; set; }
        public string? Packaging { get; set; }
        public long? PriceMax { get; set; }
        public long? PriceMin { get; set; }
        public byte[]? Image { get; set; }
        public string? ImagePath { get; set; }
        public int? Qty { get; set; }
        public long? MedicalFacilityId { get; set; }
        public long? CourirId { get; set; }
        public decimal? SubTotal { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDelete { get; set; }
        public List<VMTMedicalItemPurchaseDetail> ListDetails { get; set; }
    }
}
