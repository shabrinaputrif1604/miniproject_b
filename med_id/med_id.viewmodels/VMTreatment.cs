﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace med_id.viewmodels
{
    public class VMTreatment
    {
        public long Id { get; set; }
        public string? TreatmentName { get; set; }
        public List<VMTreatment>? treatments { get; set; }
        public long DoctorOfficeTreatmentId { get; set; }
    }
}
