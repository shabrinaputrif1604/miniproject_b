﻿using med_id.Services;
using med_id.viewmodels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NuGet.Protocol;

namespace med_id.Controllers
{
    public class AuthController : Controller
    {
        private AuthService authService;
        VMResponse response = new VMResponse();
        public AuthController(AuthService _authService)
        {
            authService = _authService;
        }
        public IActionResult Login()
        {
            return PartialView();
        }

        [HttpPost]
        public async Task<JsonResult> Login(string email, string password)
        {
            VMResponse responseLogin = await authService.CheckLogin(email, password);
            Console.WriteLine("response login = " + responseLogin.Entity.ToJson());
            if (!responseLogin.Success)
            {
                response.Success = false;
                response.Message = responseLogin.Message;
                return Json(response);
            }

            VMBiodataUser user = JsonConvert.DeserializeObject<VMBiodataUser>(responseLogin.Entity.ToString());

            HttpContext.Session.SetInt32("IdUser", (int)user.Id);
            HttpContext.Session.SetInt32("IdRole", (int)user.RoleId);
            HttpContext.Session.SetInt32("IdBiodata", (int)user.BiodataId);
            HttpContext.Session.SetString("FullName", user.NameUser);
            HttpContext.Session.SetString("NameRole", user.NameRole);
            

            response.Message = responseLogin.Message;

            return Json(response);
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }
    }
}
