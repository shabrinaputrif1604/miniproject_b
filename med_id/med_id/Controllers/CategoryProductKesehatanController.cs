﻿using med_id.datamodels;
using med_id.Services;
using med_id.viewmodels;
using Microsoft.AspNetCore.Mvc;

namespace med_id.Controllers
{
    public class CategoryProductKesehatanController : Controller
    {
        private CategoryProductKesehatanService categoryProductKesehatanService;
        private int IdUser = 1;


        public CategoryProductKesehatanController(CategoryProductKesehatanService _categoryProductKesehatanService)
        {
            categoryProductKesehatanService = _categoryProductKesehatanService;
        }

        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            List<MMedicalItemCategory> data = await categoryProductKesehatanService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Name.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Name).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Name).ToList();
                    break;
            }

            return View(PaginatedList<MMedicalItemCategory>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public IActionResult Create()
        {
            MMedicalItemCategory data = new MMedicalItemCategory();
            return PartialView(data);
        }


        [HttpPost]
        public async Task<IActionResult> Create(MMedicalItemCategory dataParam)
        {
            dataParam.CreatedBy = IdUser;
            VMResponse respon = await categoryProductKesehatanService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<JsonResult> CheckNameIsExist(string name, int id)
        {
            bool isExist = await categoryProductKesehatanService.CheckCategoryByName(name, id);
            return Json(isExist);
        }

        public async Task<IActionResult> Edit(int id)
        {
            MMedicalItemCategory data = await categoryProductKesehatanService.GetDataById(id);
            return PartialView(data);
        }


        [HttpPost]
        public async Task<IActionResult> Edit(MMedicalItemCategory dataParam)
        {
            dataParam.ModifiedBy = IdUser;
            dataParam.ModifiedOn = DateTime.Now;
            VMResponse respon = await categoryProductKesehatanService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Detail(int id)
        {
            MMedicalItemCategory data = await categoryProductKesehatanService.GetDataById(id);
            return PartialView(data);
        }

        public async Task<IActionResult> Delete(int id)
        {
            MMedicalItemCategory data = await categoryProductKesehatanService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            int createBy = IdUser;
            VMResponse respon = await categoryProductKesehatanService.Delete(id, createBy);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return RedirectToAction("Index");
        }
    }
}
