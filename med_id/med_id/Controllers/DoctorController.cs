﻿using med_id.datamodels;
using med_id.Services;
using med_id.viewmodels;
using Microsoft.AspNetCore.Mvc;

namespace med_id.Controllers
{
    public class DoctorController : Controller
    {
        private DoctorService doctorService;
        private SpesialisasiDokterService spesialisasiDokterService;


        private readonly IWebHostEnvironment webHostEnvirontment;
        public DoctorController(DoctorService _dokterService, IWebHostEnvironment _webHostEnvirontment, SpesialisasiDokterService _spesialisasiDokterService)
        {
            doctorService = _dokterService;
            webHostEnvirontment = _webHostEnvirontment;
            spesialisasiDokterService = _spesialisasiDokterService;
        }
        public async Task<IActionResult> IndexDetail(int id)
        {
            VMDoctorProfile data = await doctorService.GetDataByIdProfile(id);
            return View(data);
        }
        public async Task<IActionResult> Index(int id)
        {
            VMDoctorProfile data = await doctorService.GetDataByIdProfile(id);
            return View(data);
        }
        public string Upload(IFormFile ImageFile)
        {
            string uniqueFileName = "";

            if (ImageFile != null)
            {
                string uploadFolder = Path.Combine(webHostEnvirontment.WebRootPath, "images");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + ImageFile.FileName;
                string filePath = Path.Combine(uploadFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    ImageFile.CopyTo(fileStream);
                }
            }

            return uniqueFileName;
        }

        [HttpPost]
        public async Task<IActionResult> SaveImg(VMSavePoto dataParam)
        {
            if (dataParam.ImageFile != null)
            {
                dataParam.ImagePath = Upload(dataParam.ImageFile);
            }
            //dataParam.CreateBy = IdUser;
            VMResponse respon = await doctorService.SaveImg(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Create(int id)
        {
            VMDoctorProfile data = await doctorService.GetDataByIdProfile(id);

            List<MSpecialization> listCategory = await spesialisasiDokterService.GetAllData();
            ViewBag.ListCategory = listCategory;

            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Create(VMDoctorProfile dataParam)
        {
            dataParam.CreatedBy = 1;
            VMResponse respon = await doctorService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Edit(int id)
        {
            VMDoctorProfile data = await doctorService.GetDataByIdProfile(id);

            List<MSpecialization> listCategory = await spesialisasiDokterService.GetAllData();
            ViewBag.ListCategory = listCategory;

            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(VMDoctorProfile dataParam)
        {
            dataParam.CreatedBy = 1;
            VMResponse respon = await doctorService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }
        public async Task<JsonResult> CheckTokenIsExist(int id, string nameDay, int nameJam, int nameMenit)
        {
            string day = nameDay;
            if (day == "1")
            {
                day = "Senin";
            }
            if (day == "2")
            {
                day = "Selasa";
            }
            if (day == "3")
            {
                day = "Rabu";
            }
            if (day == "4")
            {
                day = "Kamis";
            }
            if (day == "5")
            {
                day = "Jumat";
            }
            if (day == "6")
            {
                day = "Sabtu";
            }
            if (day == "0")
            {
                day = "Minggu";
            }
            bool isExist = await doctorService.CheckJadwal(id, day, nameJam, nameMenit);
            return Json(isExist);
        }

    }
}
