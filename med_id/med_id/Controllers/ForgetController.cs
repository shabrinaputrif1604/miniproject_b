﻿using med_id.Services;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mail;
using System.Net;
using med_id.datamodels;
using med_id.viewmodels;

namespace med_id.Controllers
{
    public class ForgetController : Controller
    {
        private ForgetService forgetService;
        private DoctorService doctorService;

        public ForgetController(ForgetService _forgetService, DoctorService _doctorService)
        {
            forgetService = _forgetService;
            doctorService = _doctorService;
        }
        public async Task<JsonResult> CheckEmailIsExist(string email)
        {
            bool isExist = await forgetService.CheckByEmail(email);
            return Json(isExist);
        }
        public IActionResult SetPass()
        {
            return PartialView();
        }
        public IActionResult VerifEmail()
        {
            MUser data = new MUser();
            return PartialView(data);
        }
        public string GetOtp()
        {
            // string number = "0123456789";
            Random random = new Random();
            string otp = string.Empty;
            for (int i = 0; i < 6; i++)
            {
                int addrandom = random.Next(0, 9);
                otp += addrandom;
            }
            return otp;
        }
        [HttpPost]
        public async Task<IActionResult> GenerateOTP(VMResetPassOtp dataParam)
        {
            var kodeOTP = GetOtp();
            dataParam.Token = kodeOTP;
            dataParam.UsedFor = "lupa Password";
            //Send Kode OTP via Email
            var fromMail = new MailAddress("a.setiawan1501@gmail.com", "Admin");
            var fromEmailpassword = "bdvi wdlb abho ttcw";
            var toMail = new MailAddress(dataParam.Email);
            //var toMail = new MailAddress("axew150199@gmail.com");

            var smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(fromMail.Address, fromEmailpassword);

            var Message = new MailMessage(fromMail, toMail);
            Message.Subject = "Password Reset";
            Message.Body = "<br/> OTP : " + kodeOTP;
            Message.IsBodyHtml = true;

            smtp.Send(Message);
            VMResponse respon = await forgetService.GenerateToken(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return PartialView(dataParam);
        }
        [HttpPost]
        public async Task<IActionResult> UpdateOtp(TToken dataParam)
        {
            
            VMResponse respon = await forgetService.UpdateOtp(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }
        public IActionResult ConfirmOtp(string email)
        {
            ViewBag.Email = email;
            return PartialView();
        }
        public async Task<JsonResult> CheckTokenIsExist(string email, string otp)
        {
            bool isExist = await forgetService.CheckByEmailToken(email, otp);
            return Json(isExist);
        }
        public async Task<JsonResult> CheckTokenExp(string email, string otp)
        {
            bool isExist = await forgetService.CheckTokenExp(email, otp);
            return Json(isExist);
        }
        public IActionResult PassConfirm(string email)
        {
            ViewBag.Email = email;
            return PartialView();
        }
        [HttpPost]
        public async Task<IActionResult> UpdatePass(VMResetPassOtp dataParam)
        {
            VMResponse respon = await forgetService.UpdatePass(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }
        [HttpPost]
        public async Task<IActionResult> ReturnSend(TToken dataParam)
        {
            var kodeOTP = GetOtp();
            dataParam.Token = kodeOTP;
            dataParam.UsedFor = "lupa Password";
            //Send Kode OTP via Email
            var fromMail = new MailAddress("a.setiawan1501@gmail.com", "Pukimak");
            var fromEmailpassword = "bdvi wdlb abho ttcw";
            var toMail = new MailAddress(dataParam.Email);
            //var toMail = new MailAddress("axew150199@gmail.com");

            var smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(fromMail.Address, fromEmailpassword);

            var Message = new MailMessage(fromMail, toMail);
            Message.Subject = "Password Reset";
            Message.Body = "<br/> OTP : " + kodeOTP;
            Message.IsBodyHtml = true;

            smtp.Send(Message);
            VMResponse respon = await forgetService.ReturnSend(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return PartialView(dataParam);
        }
        

    }
}
