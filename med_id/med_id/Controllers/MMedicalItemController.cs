﻿using med_id.datamodels;
using med_id.Services;
using med_id.viewmodels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace med_id.Controllers
{
    public class MMedicalItemController : Controller
    {
        private CategoryProductKesehatanService categoryProductKesehatanService;
        private SegmentationProductKesehatanService segmentationProductKesehatanService;
        private MMedicalItemService mMedicalItemService;
        private readonly IWebHostEnvironment webHostEnvirontment;
        private int IdUser = 1;

        public MMedicalItemController(CategoryProductKesehatanService _categoryProductKesehatanService, SegmentationProductKesehatanService _segmentationProductKesehatanService, MMedicalItemService _mMedicalItemService, IWebHostEnvironment _webHostEnvirontment)
        {
            categoryProductKesehatanService = _categoryProductKesehatanService;
            segmentationProductKesehatanService = _segmentationProductKesehatanService;
            mMedicalItemService = _mMedicalItemService;
            webHostEnvirontment = _webHostEnvirontment; 
        }

        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.PriceSort = sortOrder == "price" ? "price_desc" : "price";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            List<VMMMedicalItem> data = await mMedicalItemService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Name.ToLower().Contains(searchString.ToLower())
                    || a.NameCategory.ToLower().Contains(searchString.ToLower())
                    || a.NameSegmentation.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Name).ToList();
                    break;
                case "price_desc":
                    data = data.OrderByDescending(a => a.PriceMin).ToList();
                    break;
                case "price":
                    data = data.OrderBy(a => a.PriceMin).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Name).ToList();
                    break;
            }

            return View(PaginatedList<VMMMedicalItem>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public async Task<IActionResult> Create()
        {
            VMMMedicalItem data = new VMMMedicalItem();

            List<MMedicalItemCategory> listCategory = await categoryProductKesehatanService.GetAllData();
            ViewBag.ListCategory = listCategory;

            List<MMedicalItemSegmentation> listVariant = await segmentationProductKesehatanService.GetAllData();
            ViewBag.listVariant = listVariant;

            return PartialView(data);
        }

        public async Task<JsonResult> CheckNameIsExist(string name, int id, int medicalItemSegmentationId, int medicalItemCategoryId)
        {
            bool isExist = await mMedicalItemService.CheckByName(name, id, medicalItemSegmentationId, medicalItemCategoryId);
            return Json(isExist);
        }

        [HttpPost]
        public async Task<IActionResult> Create(VMMMedicalItem dataParam)
        {
            if (dataParam.ImageFile != null)
            {
                dataParam.ImagePath = Upload(dataParam.ImageFile);
            }
            //dataParam.CreateBy = IdUser;
            VMResponse respon = await mMedicalItemService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public string Upload(IFormFile ImageFile)
        {
            string uniqueFileName = "";

            if (ImageFile != null)
            {
                string uploadFolder = Path.Combine(webHostEnvirontment.WebRootPath, "images");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + ImageFile.FileName;
                string filePath = Path.Combine(uploadFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    ImageFile.CopyTo(fileStream);
                }
            }

            return uniqueFileName;
        }

        public async Task<IActionResult> Edit(int id)
        {
            VMMMedicalItem data = await mMedicalItemService.GetDataById(id);

            List<MMedicalItemCategory> listCategory = await categoryProductKesehatanService.GetAllData();
            ViewBag.ListCategory = listCategory;

            List<MMedicalItemSegmentation> listVariant = await segmentationProductKesehatanService.GetAllData();
            ViewBag.listVariant = listVariant;

            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(VMMMedicalItem dataParam)
        {
            if (dataParam.ImageFile != null)
            {
                dataParam.ImagePath = Upload(dataParam.ImageFile);
            }
            //dataParam.CreateBy = IdUser;
            VMResponse respon = await mMedicalItemService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Detail(int id)
        {
            VMMMedicalItem data = await mMedicalItemService.GetDataById(id);
            return PartialView(data);
        }

        public async Task<IActionResult> Delete(int id)
        {
            VMMMedicalItem data = await mMedicalItemService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            VMResponse respon = await mMedicalItemService.Delete(id);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return RedirectToAction("Index");
        }
    }
}
