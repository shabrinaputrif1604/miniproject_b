﻿using med_id.datamodels;
using med_id.Services;
using med_id.viewmodels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace med_id.Controllers
{
    public class ObatController : Controller
    {
        private MMedicalItemService mMedicalItemService;
        private CategoryProductKesehatanService categoryProductKesehatanService;
        private ObatHeaderService obatHeaderService;


        public ObatController(MMedicalItemService _mMedicalItemService, ObatHeaderService _obatHeaderService, CategoryProductKesehatanService _categoryProductKesehatanService)
        {
            mMedicalItemService = _mMedicalItemService;
            obatHeaderService = _obatHeaderService;
            categoryProductKesehatanService = _categoryProductKesehatanService;

        }

        public async Task<IActionResult> Catalog(VMSearchPage dataSearch)
        {
            List<VMMMedicalItem> dataProduct = await mMedicalItemService.GetAllData();
            dataSearch.PriceMin = dataSearch.PriceMin == null ? long.MinValue : dataSearch.PriceMin;
            dataSearch.PriceMax = dataSearch.PriceMax == null ? long.MaxValue : dataSearch.PriceMax;
            //dataSearch.MaxAmount = dataSearch.MaxAmount ?? decimal.MaxValue;//jika null data decimal jika tidak maka data searchnya sendiri

            if (dataSearch.NameProduct != null)
            {
                dataProduct = dataProduct.Where(a => a.Name.ToLower().Contains(dataSearch.NameProduct.ToLower())).ToList();
            }
            //if (dataSearch.NameCategory != null)
            //{
            //    dataProduct = dataProduct.Where(a => a.NameCategory.ToLower().Contains(dataSearch.NameCategory.ToLower())).ToList();
            //}

            //if (dataSearch.PriceMin != null && dataSearch.PriceMax != null)
            //{
            //    dataProduct = dataProduct.Where(a => a.PriceMin >= dataSearch.PriceMin
            //                  && a.PriceMax <= dataSearch.PriceMax).ToList();
            //}
            if (dataSearch.Caution != null)
            {
                if (dataSearch.Caution.ToLower() == "keras,bebas")
                {
                    dataProduct = dataProduct
                    .Where(a => a.Caution.ToLower().Contains("bebas") || a.Caution.ToLower().Contains("keras"))
                    .ToList();
                }
                else
                {
                    dataProduct = dataProduct.Where(a => a.Caution.ToLower().Contains(dataSearch.Caution.ToLower())).ToList();
                }
            }
            if (dataSearch.MedicalItemCategoryId != null)
            {
                dataProduct = dataProduct.Where(a => a.MedicalItemCategoryId == dataSearch.MedicalItemCategoryId).ToList();
            }

            ViewBag.Search = dataSearch;
            ViewBag.CurrentPageSize = dataSearch.PageSize;

            //Get session in VMOrderHeader first load
            VMTMedicalItemPurchaseDetail dataHeader = HttpContext.Session.GetComplexData<VMTMedicalItemPurchaseDetail>("ListCart");
            if (dataHeader == null)
            {
                dataHeader = new VMTMedicalItemPurchaseDetail();
                dataHeader.ListDetails = new List<VMTMedicalItemPurchaseDetail>();
            }

            var ListDetail = JsonConvert.SerializeObject(dataHeader.ListDetails);
            ViewBag.dataHeader = dataHeader;
            ViewBag.dataDetail = ListDetail;

            ViewBag.Search = dataSearch;
            ViewBag.CurrentPageSize = dataSearch.PageSize;

            return View(PaginatedList<VMMMedicalItem>.CreateAsync(dataProduct, dataSearch.PageNumber ?? 1, dataSearch.PageSize ?? 4));
        }

        public async Task<IActionResult> CariObat()
        {
            List<MMedicalItemCategory> listCategory = await categoryProductKesehatanService.GetAllData();

            // Filter out null Name values if necessary
            listCategory = listCategory.Where(category => category.Name != null).ToList();

            ViewBag.listCategory = listCategory;
            return View();
        }

        [HttpPost]
        public JsonResult SetSession(VMTMedicalItemPurchaseDetail dataHeader)
        {
            HttpContext.Session.SetComplexData("ListCart", dataHeader);
            return Json("");
        }

        public JsonResult RemoveSession()
        {
            HttpContext.Session.Remove("ListCart");
            //HttpContext.Session.Clear(); //Clear all session
            return Json("");
        }
    }
}
