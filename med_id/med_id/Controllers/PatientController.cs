﻿using med_id.datamodels;
using med_id.Services;
using med_id.viewmodels;
using Microsoft.AspNetCore.Mvc;

namespace med_id.Controllers
{
    public class PatientController : Controller
    {
        private PatientService patientService;

        public PatientController(PatientService _patientService)
        {
            patientService = _patientService;
        }
        public IActionResult Index()
        {
            return View();
        }

        //view ini mereturn data kumpulan dokter
        public async Task<IActionResult> FindDoctor()
        {
            List<VMFindDoctor> data = await patientService.GetAllData();
            return View(data);
        }

        public async Task<JsonResult> GetDataDays(int id, int medicalFacilityId)
        {
            List<VMSchedule> dataSchedule = await patientService.GetDataDaysById(id, medicalFacilityId);
            return Json(dataSchedule);
        }

        public async Task<IActionResult> MakeAppointment(int id, int idPatient, int medicalFacilityId)
        {
            VMFindDoctor data = await patientService.GetDataById(id);

            //butuh data family member 
            List<VMFamilyMember> dataFamily = await patientService.GetDataByIdParent(idPatient);
            ViewBag.listDataFamily = dataFamily;
            //butuh data medical facility
            VMFindDoctor dataMedFacility = await patientService.GetDataMedicalFacility(id);
            ViewBag.dataMedFacility = dataMedFacility;

            //butuh data medical treatment
            VMTreatment dataTreatment = await patientService.GetDataTreatmentById(id);
            ViewBag.treatment = dataTreatment;

            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> CreateAppointment(VMFindDoctor dataParam)
        {
            List<VMFullSlotDate> responseSlot = await patientService.GetFullSlotDate(dataParam.DoctorId);
            //cek tanggal di dataParam dgn tanggal responseSlot, kalo ada brrti false bisa simpan data
            //Console.WriteLine(dataParam.AppointmentDate.Date);
            
            for(int i = 0; i < responseSlot.Count(); i++)
            {
            //Console.WriteLine(responseSlot[i].AppointmentDate.Value.Date);
                if(dataParam.AppointmentDate.Date == responseSlot[i].AppointmentDate.Value.Date)
                {
                    VMResponse response = new VMResponse();
                    response.Success = false;
                    response.Message = "Slot sudah penuh";

                    return Json(response);
                }
            }
            //kalau penuh, mau nampilin modal load slot penuh 

            VMResponse responseAppointment = await patientService.MakeAppointment(dataParam);

            if (responseAppointment.Success)
            {
                return Json(responseAppointment);
            }

            return View(dataParam);
        }

        public async Task<IActionResult> DoctorProfile(int id)
        {
            VMFindDoctor data = await patientService.GetDataById(id);

            VMFindDoctor dataMedFacility = await patientService.GetDataMedicalFacility(id);
            ViewBag.dataMedFacility = dataMedFacility;

            return View(data);
        }

        public async Task<IActionResult> MakeAppointmentById(int id, int idPatient, int medicalFacilityId)
        {
            VMFindDoctor data = await patientService.GetDataById(id);

            //butuh data family member 
            List<VMFamilyMember> dataFamily = await patientService.GetDataByIdParent(idPatient);
            ViewBag.listDataFamily = dataFamily;
            //butuh data medical facility
            VMFindDoctor dataMedFacility = await patientService.GetDataMedicalFacility(id);
            ViewBag.dataMedFacility = dataMedFacility;

            //butuh data tanggal kedatangan (m_medical_facility_schedule)
            //List<VMSchedule> dataSchedule = await patientService.GetDataDaysById(id, medicalFacilityId);
            //ViewBag.schedule = dataSchedule;

            //butuh data medical treatment
            VMTreatment dataTreatment = await patientService.GetDataTreatmentById(id);
            ViewBag.treatment = dataTreatment;

            ViewBag.medicalFacilityId = medicalFacilityId;

            return PartialView(data);
        }

        public async Task<IActionResult> EditAppointment(int id, int idPatient, int appointmentId)
        {
            //data dokter
            VMFindDoctor data = await patientService.GetDataById(id);

            //butuh data family member 
            List<VMFamilyMember> dataFamily = await patientService.GetDataByIdParent(idPatient);
            ViewBag.listDataFamily = dataFamily;

            //butuh data medical facility
            VMFindDoctor dataMedFacility = await patientService.GetDataMedicalFacility(id);
            ViewBag.dataMedFacility = dataMedFacility;

            ////butuh data tanggal kedatangan (m_medical_facility_schedule)
            //List<VMSchedule> dataSchedule = await patientService.GetDataDaysById(id, medicalFacilityId);
            //ViewBag.schedule = dataSchedule;

            //butuh data medical treatment
            VMTreatment dataTreatment = await patientService.GetDataTreatmentById(id);
            ViewBag.treatment = dataTreatment;

            ViewBag.appointmentId = appointmentId;

            VMAppointments dataAppointment = await patientService.GetAppointmentById(appointmentId);
            ViewBag.dataAppointment = dataAppointment;

            return PartialView(data);
        }

        public async Task<JsonResult> GetFullSlotDate(int id)
        {
            List<VMFullSlotDate> data = await patientService.GetFullSlotDate(id);
            return Json(data);
        }

        public async Task<IActionResult> FullSlot()
        {
            return PartialView();
        }

        public async Task<IActionResult> Appointments()
        {
            int IdBiodata = HttpContext.Session.GetInt32("IdBiodata") ?? 5;
            List<VMAppointments> data = await patientService.GetAppointments(IdBiodata);
            return View(data);
        }

        [HttpPut]
        public async Task<IActionResult> EditAppointment(int idAppointment, VMFindDoctor dataParam)
        {
            List<VMFullSlotDate> responseSlot = await patientService.GetFullSlotDate(dataParam.DoctorId);
            //cek tanggal di dataParam dgn tanggal responseSlot, kalo ada brrti false bisa simpan data
            //Console.WriteLine(dataParam.AppointmentDate.Date);

            for (int i = 0; i < responseSlot.Count(); i++)
            {
                //Console.WriteLine(responseSlot[i].AppointmentDate.Value.Date);
                if (dataParam.AppointmentDate.Date == responseSlot[i].AppointmentDate.Value.Date)
                {
                    VMResponse response = new VMResponse();
                    response.Success = false;
                    response.Message = "Slot sudah penuh";

                    return Json(response);
                }
            }
            //kalau penuh, mau nampilin modal load slot penuh 

            VMResponse responseAppointment = await patientService.EditAppointment(idAppointment, dataParam);

            if (responseAppointment.Success)
            {
                return Json(responseAppointment);
            }

            return View(dataParam);
        }
    }
}
