﻿using med_id.datamodels;
using med_id.Services;
using med_id.viewmodels;
using Microsoft.AspNetCore.Mvc;

namespace med_id.Controllers
{
    public class PendidikanController : Controller
    {
        PendidikanService pendidikanService;
        public PendidikanController(PendidikanService _pendidikanService)
        {
            pendidikanService = _pendidikanService;
        }

        public async Task<IActionResult>Index1(string sortOrder, string searchString)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
           
            ViewBag.CurrentFilter = searchString;
            List<MEducationLevel> data = await pendidikanService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Name.ToLower().Contains(searchString.ToLower())).ToList();
            }
            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Name).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Name).ToList();
                    break;
            }

            return View(data);
        }
        public IActionResult Create()
        {
            MEducationLevel data = new MEducationLevel();
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Create(MEducationLevel dataParam)
        {
            dataParam.CreatedBy = 1;
            VMResponse respon = await pendidikanService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<JsonResult> CheckNameIsExsist(string name, int id)
        {
            bool isExist = await pendidikanService.CheckName(name, id);
            return Json(isExist);
        }

        public async Task<IActionResult> Edit(int id)
        {
            MEducationLevel data = await pendidikanService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(MEducationLevel dataParam)
        {
            dataParam.ModifiedBy = 1;
            VMResponse respon = await pendidikanService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Detail(int id)
        {
            MEducationLevel data = await pendidikanService.GetDataById(id);
            return PartialView(data);
        }

        /*      [HttpGet("HapusData")]*/
        public async Task<IActionResult> Delete(int id)
        {
            MEducationLevel data = await pendidikanService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            int modifedby = 1;
            VMResponse respon = await pendidikanService.Delete(id, modifedby);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return RedirectToAction("Index");
        }
    }
}
