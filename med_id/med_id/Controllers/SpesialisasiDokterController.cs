﻿using med_id.datamodels;
using med_id.Services;
using med_id.viewmodels;
using Microsoft.AspNetCore.Mvc;

namespace med_id.Controllers
{
    public class SpesialisasiDokterController : Controller
    {
        private SpesialisasiDokterService spesialisasiDokterService;
        private int IdUser = 1;

        public SpesialisasiDokterController(SpesialisasiDokterService _spesialisasiDokterService)
        {
            spesialisasiDokterService = _spesialisasiDokterService;
        }
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            List<MSpecialization> data = await spesialisasiDokterService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Name.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Name).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Name).ToList();
                    break;
            }

            return View(PaginatedList<MSpecialization>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public IActionResult Create()
        {
            MSpecialization data = new MSpecialization();
            return PartialView(data);
        }


        [HttpPost]
        public async Task<IActionResult> Create(MSpecialization dataParam)
        {
            dataParam.CreatedBy = IdUser;
            VMResponse respon = await spesialisasiDokterService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<JsonResult> CheckNameIsExist(string name, int id)
        {
            bool isExist = await spesialisasiDokterService.CheckCategoryByName(name, id);
            return Json(isExist);
        }

        public async Task<IActionResult> Edit(int id)
        {
            MSpecialization data = await spesialisasiDokterService.GetDataById(id);
            return PartialView(data);
        }


        [HttpPost]
        public async Task<IActionResult> Edit(MSpecialization dataParam)
        {
            dataParam.ModifiedBy = IdUser;
            dataParam.ModifiedOn = DateTime.Now;
            VMResponse respon = await spesialisasiDokterService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Delete(int id)
        {
            MSpecialization data = await spesialisasiDokterService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            int createBy = IdUser;
            VMResponse respon = await spesialisasiDokterService.Delete(id, createBy);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return RedirectToAction("Index");
        }
    }
}
