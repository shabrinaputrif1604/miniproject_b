﻿using med_id.viewmodels;
using Newtonsoft.Json;

namespace med_id.Services
{
    public class AuthService
    {
        private static readonly HttpClient client = new HttpClient();
        private string RouteApi = "";
        private VMResponse response = new VMResponse();
        private IConfiguration config;
        public AuthService(IConfiguration _config)
        {
            config = _config;
            RouteApi = config["RouteAPI"];
        }

        public async Task<VMResponse> CheckLogin(string email, string password)
        {
            //VMBiodataUser data = new VMBiodataUser();

            string apiRequest = await client.GetStringAsync(RouteApi + $"api/Auth/Login/{email}/{password}");

            VMResponse data = JsonConvert.DeserializeObject<VMResponse>(apiRequest);

            return data;
        }

        public async Task<List<VMMenu>> MenuAccess(int idRole)
        {
            List<VMMenu> data = new List<VMMenu>();

            string apiRequest = await client.GetStringAsync(RouteApi + $"api/Auth/Menu/{idRole}");

            data = JsonConvert.DeserializeObject<List<VMMenu>>(apiRequest)!;

            return data;
        }
    }
}
