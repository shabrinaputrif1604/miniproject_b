﻿using med_id.datamodels;
using med_id.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace med_id.Services
{
    public class DoctorService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public DoctorService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }
        public async Task<VMDoctorProfile> GetDataByIdProfile(int id)
        {
            VMDoctorProfile data = new VMDoctorProfile();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"Profile/GetDataProfilDoctorById/{id}");
            data = JsonConvert.DeserializeObject<VMDoctorProfile>(apiResponse)!;
            return data;
        }
        public async Task<MDoctor> GetDataDoctor(int idBio)
        {
            MDoctor data = new MDoctor();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"Profile/GetDataDoctor/{idBio}");
            data = JsonConvert.DeserializeObject<MDoctor>(apiResponse)!;
            return data;
        }
        public async Task<VMResponse> SaveImg(VMSavePoto data)
        {
            string json = JsonConvert.SerializeObject(data);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PostAsync(RouteAPI + "Profile/Save", content);
            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Edit(VMDoctorProfile dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "Profile/Edit", content);
            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
        public async Task<bool> CheckJadwal(int id, string nameDay, int nameJam, int nameMenit)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"Profile/CheckJadwal/{id}/{nameDay}/{nameJam}/{nameMenit}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiRespon);

            return isExist;
        }
    }
}
