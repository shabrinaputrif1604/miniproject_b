﻿using med_id.datamodels;
using med_id.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace med_id.Services
{
    public class ForgetService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public ForgetService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }
        public async Task<List<MUser>> GetAllData()
        {
            List<MUser> data = new List<MUser>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "Forget/GetAllData");
            data = JsonConvert.DeserializeObject<List<MUser>>(apiResponse);

            return data;

        }
        public async Task<bool> CheckByEmail(string email)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"Forget/CheckEmail/{email}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiRespon);

            return isExist;
        }
        public async Task<VMResponse> GenerateToken(VMResetPassOtp dataParam)
        {
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PostAsync(RouteAPI + "Forget/Save", content);
            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
        public async Task<VMResponse> ReturnSend(TToken dataParam)
        {
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PostAsync(RouteAPI + "Forget/ReturnSend", content);
            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
        public async Task<bool> CheckByEmailToken(string email, string otp)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"Forget/CheckEmailToken/{email}/{otp}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiRespon);

            return isExist;
        }
        public async Task<bool> CheckTokenExp(string email, string otp)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"Forget/CheckTokenExp/{email}/{otp}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiRespon);

            return isExist;
        }
        public async Task<VMResponse> UpdateOtp(TToken dataParam)
        {
            //Proses convert dari objek ke string format json
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu dikirim sebagai requast bofy
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil api dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "Forget/UpdateOtp", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari api
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari api ke objek
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;

        }
        public async Task<VMResponse> UpdatePass(VMResetPassOtp dataParam)
        {
            //Proses convert dari objek ke string format json
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu dikirim sebagai requast bofy
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil api dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "Forget/UpdatePass", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari api
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari api ke objek
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;

        }
    }
}
