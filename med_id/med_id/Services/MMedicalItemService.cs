﻿using med_id.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace med_id.Services
{
    public class MMedicalItemService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public MMedicalItemService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<VMMMedicalItem>> GetAllData()
        {
            List<VMMMedicalItem> data = new List<VMMMedicalItem>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiMedicalItem/GetAllData");
            data = JsonConvert.DeserializeObject<List<VMMMedicalItem>>(apiResponse)!;

            return data;
        }

        public async Task<bool> CheckByName(string name, int id, int medicalItemSegmentationId, int medicalItemCategoryId)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiMedicalItem/CheckByName/{name}/{id}/{medicalItemSegmentationId}/{medicalItemCategoryId}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiRespon);
            return isExist;
        }

        public async Task<VMResponse> Create(VMMMedicalItem dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiMedicalItem/Save", content);
            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMMMedicalItem> GetDataById(int id)
        {
            VMMMedicalItem data = new VMMMedicalItem();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiMedicalItem/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMMMedicalItem>(apiResponse);

            return data;
        }

        public async Task<VMResponse> Edit(VMMMedicalItem dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiMedicalItem/Edit", content);
            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiMedicalItem/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;

        }
    }
}
