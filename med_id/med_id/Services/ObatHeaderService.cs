﻿using med_id.viewmodels;
using Newtonsoft.Json;


namespace med_id.Services
{
    public class ObatHeaderService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public ObatHeaderService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<VMTMedicalItemPurchaseDetail>> GetAllData()
        {
            List<VMTMedicalItemPurchaseDetail> data = new List<VMTMedicalItemPurchaseDetail>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiObat/GetAllDataOrderHeader");
            data = JsonConvert.DeserializeObject<List<VMTMedicalItemPurchaseDetail>>(apiResponse)!;

            return data;
        }
    }
}
