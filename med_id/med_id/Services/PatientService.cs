﻿using med_id.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace med_id.Services
{
    public class PatientService
    {
        private static readonly HttpClient client = new HttpClient();
        private string RouteApi = "";
        private IConfiguration config;
        private VMResponse response = new VMResponse();

        public PatientService(IConfiguration _config)
        {
            config = _config;
            RouteApi = config["RouteAPI"];
        }

        public async Task<List<VMFindDoctor>> GetAllData()
        {
            string apiRequest = await client.GetStringAsync(RouteApi + "api/patient");
            List<VMFindDoctor> data = JsonConvert.DeserializeObject<List<VMFindDoctor>>(apiRequest);
            return data;
        }

        public async Task<VMFindDoctor> GetDataById(int id)
        {
            string apiRequest = await client.GetStringAsync(RouteApi + "api/patient/" + id);
            VMFindDoctor data = JsonConvert.DeserializeObject<VMFindDoctor>(apiRequest);
            return data;
        }

        public async Task<List<VMFamilyMember>> GetDataByIdParent(int idParent)
        {
            string apiRequest = await client.GetStringAsync(RouteApi + "api/patient/family/" + idParent);
            List<VMFamilyMember> data = JsonConvert.DeserializeObject<List<VMFamilyMember>>(apiRequest);
            return data;
        }

        public async Task<VMFindDoctor> GetDataMedicalFacility(int id)
        {
            string apiRequest = await client.GetStringAsync(RouteApi + "api/patient/medicalFacility/" + id);
            VMFindDoctor data = JsonConvert.DeserializeObject<VMFindDoctor>(apiRequest);
            return data;
        }

        public async Task<VMTreatment> GetDataTreatmentById(int id)
        {
            string apiRequest = await client.GetStringAsync(RouteApi + "api/patient/treatment/" + id);
            VMTreatment data = JsonConvert.DeserializeObject<VMTreatment>(apiRequest);
            return data;
        }

        public async Task<List<VMSchedule>> GetDataDaysById(int id, int medicalFacilityId)
        {
            string apiRequest = await client.GetStringAsync(RouteApi + $"api/patient/schedule/{id}/{medicalFacilityId}");
            List<VMSchedule> data = JsonConvert.DeserializeObject<List<VMSchedule>>(apiRequest);
            return data;
        }

        public async Task<VMResponse> MakeAppointment(VMFindDoctor dataParam)
        {
            string json = JsonConvert.SerializeObject(dataParam);

            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            var request = await client.PostAsync(RouteApi + "api/patient/makeAppointment", content);

            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();

                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse);
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return response;
        }

        public async Task<List<VMFullSlotDate>> GetFullSlotDate(long id)
        {
            string apiRequest = await client.GetStringAsync(RouteApi + "api/patient/fullSlotDate/" + id);
            List<VMFullSlotDate> data = JsonConvert.DeserializeObject<List<VMFullSlotDate>>(apiRequest);
            return data;
        }

        public async Task<List<VMAppointments>> GetAppointments(int id)
        {
            string apiRequest = await client.GetStringAsync(RouteApi + "api/patient/appointments/" + id);
            List<VMAppointments> data = JsonConvert.DeserializeObject<List<VMAppointments>>(apiRequest);
            return data;
        }

        //public async Task<VMAppointments> EditAppointment(int idCustomer)
        //{
        //    string apiRequest = await client.GetStringAsync(RouteApi + "api/patient/appointment/" + idCustomer);
        //    VMAppointments data = JsonConvert.DeserializeObject<VMAppointments>(apiRequest);
        //    return data;
        //}

        public async Task<VMAppointments> GetAppointmentById(int idAppointment)
        {
            string apiRequest = await client.GetStringAsync(RouteApi + "api/patient/appointment/" + idAppointment);
            VMAppointments data = JsonConvert.DeserializeObject<VMAppointments>(apiRequest);
            return data;
        }

        public async Task<VMResponse> EditAppointment(int idAppointment, VMFindDoctor dataParam)
        {
            string json = JsonConvert.SerializeObject(dataParam);

            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            var request = await client.PutAsync(RouteApi + "api/patient/editAppointment/" + idAppointment, content);

            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();

                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse);
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return response;
        }
    }
}
